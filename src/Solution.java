/**
 * White Walkers Team
 * Tron-Bot 2013
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

enum Direction {
        NORTH,
        EAST,
        SOUTH,
        WEST,
}

//Culori folosite pentru algoritmii de explorare a grafurilor
enum Colour {
        BLACK,
        GREY,
        WHITE
}

//Detinatorul unei casute. Prin detinator intelegem bot-ul care ajunge primul la casuta.
enum Owner {
        NEUTRAL,
        PLAYER1,
        PLAYER2
}

class Pair<K, V> {
        public K a;
        public V b;
}

class Position {
        //Reprezinta linia (prima coordonata) a unei matrice.
        private int line;
        //Reprezinta coloana (a doua coordonata) a unei matrice.
        private int column;

        //Creeaza o instanta noua a lui Position, cu ambele coordonate
        //setate la 0.
        public Position() {
                line = column = 0;
        }

        //Creeaza o instanta noua a lui Position, initializand coordonatele
        //la valorile primite ca parametru.
        public Position(int ln, int col) {
                line = ln;
                column = col;
        }

        //Creeaza o instanta noua a lui Position, facand o copie a unei
        //instante mai vechi.
        public Position(Position old) {
                line = old.getLine();
                column = old.getColumn();
        }

        //Intoarce linia.
        public int getLine() {
                return line;
        }

        //Intoarce coloana.
        public int getColumn() {
                return column;
        }

        //Intoarce cei 8 vecini ai unui punct (nord, nord-est ...).
        public ArrayList<Position> getAllNeighbours() {
                ArrayList<Position> neighbours = new ArrayList<Position>();
                for(Direction d : Direction.values())
                        neighbours.add(getNeighbour(d));
                return neighbours;
        }

        //Intoarce coordonata vecina aflata la directia data ca parametru.
        public Position getNeighbour(Direction d) {
                if(d == Direction.NORTH)
                        return new Position(line - 1, column);
                if(d == Direction.EAST)
                        return new Position(line, column + 1);
                if(d == Direction.SOUTH)
                        return new Position(line + 1, column);
                return new Position(line, column - 1);
        }
}

class Arena {
        //Simbolul primului jucator
        public final static char player1Symbol = 'r';
        //Simbolul celui de al doilea jucator
        public final static char player2Symbol = 'g';
        //Simbolul obstacolului
        public final static char obstacleSymbol = '#';
        //Simbolul casutei goale
        public final static char emptySymbol = '-';
        //Simbolul unei casute care iese din zona de joc
        public final static char outSymbol = 'x';

        //Harta reprezentata ca o matrice de caractere
        private char[][] map;
        //Inaltimea hartii (nr de linii)
        private int height;
        //Latimea hartii (nr de coloane)
        private int width;
        //Pozitia primului jucator
        private Position player1;
        //Pozitia celui de al doilea jucator
        private Position player2;

        //Constructor pentru crearea unei arene noi, pornind de la datele de intrare
        public Arena(char[][] m, int h, int w, Position p1, Position p2) {
                height  = h;
                width   = w;
                map = new char[h][w];
                //Fac o copie a matricei primite ca parametru
                for(int i = 0; i < h; i++)
                        for(int j = 0; j < w; j++)
                                map[i][j] = m[i][j];
                player1 = new Position(p1);
                player2 = new Position(p2);
        }

        //Constructor de copiere a unei arene
        public Arena(Arena ar) {
                height = ar.getHeight();
                width = ar.getWidth();
                map = ar.getMap();
                player1 = ar.getPlayerPosition(Owner.PLAYER1);
                player2 = ar.getPlayerPosition(Owner.PLAYER2);
        }

        //Intoarce o arena noua, rezultata in urma mutarii jucatorului player in direactia d
        public Arena move(Owner player, Direction d) {
                //Calculeaza pozitia rezultata in urma miscarii
                Position nextPos = getPlayerPosition(player).getNeighbour(d);
                //Verifica validitatea miscarii
                if(getTile(nextPos) != Arena.emptySymbol || player == Owner.NEUTRAL) {
                        System.err.println("Cannot make move. Direction is invalid or player is neutral.\n");
                        return null;
                }
                //Gasesc pozitiile celor doi jucatori. Unul din cei doi este cel care se misca
                //Aceste pozitii sunt folosite pentru crearea noii arena
                Position p1;
                Position p2;
                if(player == Owner.PLAYER1) {
                        p1 = nextPos;
                        p2 = player2;
                }
                else {
                        p1 = player1;
                        p2 = nextPos;
                }
                //Face un back up la caracterul de la pozitia care trebuie schimbata
                char backUp = map[nextPos.getLine()][nextPos.getColumn()];
                //Schimba caracterul de la acea pozitie
                map[nextPos.getLine()][nextPos.getColumn()] = (player == Owner.PLAYER1 ? 
                                                                          player1Symbol :
                                                                          player2Symbol);
                //Creeaza o arena noua cu schimbarea facuta
                Arena changedArena = new Arena(map, height, width, p1, p2);
                //Schimb caracterul din harta originala la loc.
                map[nextPos.getLine()][nextPos.getColumn()] = backUp;
                return changedArena;
        }

        //Intoarce inaltimea hartii
        public int getHeight() {
                return height;
        }

        //Intoarce latimea hartii
        public int getWidth() {
                return width;
        }

        //Intoarce o copie a hartii
        public char[][] getMap() {
                char[][] mapCopy = new char[height][width];
                for(int i = 0; i < height; i++)
                        for(int j = 0; j < width; j++)
                                mapCopy[i][j] = map[i][j];
                return mapCopy;
        }

        //Intoarce o copie a pozitiei unui jucator (jucatorul 1 sau 2)
        public Position getPlayerPosition(Owner player) {
                if(player == Owner.PLAYER1)
                        return new Position(player1);
                else if(player == Owner.PLAYER2)
                        return new Position(player2);
                else
                        return null;
        }

        //Intoarce valoarea casutei aflata la pozitia data prin parametru
        public char getTile(Position pos) {
                if(pos.getLine() >= 0 && pos.getLine() < height &&
                   pos.getColumn() >= 0 && pos.getColumn() < width)
                        return map[pos.getLine()][pos.getColumn()];
                return outSymbol;
        }

        //Intoarce vecinii liberi ai unei pozitii din matrice
        public ArrayList<Direction> getFreeNeighbours(Position pos) {
                ArrayList<Direction> moves = new ArrayList<Direction>();
                for(Direction d : Direction.values()) {
                        //Pozitia in care ma uit sa vad daca este goala
                        Position neighPos = pos.getNeighbour(d);
                        if(getTile(neighPos) == emptySymbol)
                                moves.add(d);
                }
                return moves;
        }

        //Impart graful in componente voronoi, in functie de cei doi jucatori.
        //Punctele la care cei doi nu pot ajunge sunt considerate neutre.
        public Pair<Node[][], Pair<Integer, Integer> > computeTerritories(Position p1Pos,
                                                                           Position p2Pos) {
                Node[][] p1Graph = connectedComponent(p1Pos, Owner.PLAYER1);
                Node[][] p2Graph = connectedComponent(p2Pos, Owner.PLAYER2);
                Node[][] resultGraph = new Node[height][width];
                int p1Area = 0;
                int p2Area = 0;
                for(int i = 0; i < height; i++)
                        for(int j = 0; j < width; j++)
                                //Jucatorul 1 ajunge primul la acest nod, deci el este detinatorul
                                if(p1Graph[i][j].getDiscoverTime() <
                                   p2Graph[i][j].getDiscoverTime()) {
                                        resultGraph[i][j] = new Node(p1Graph[i][j]);
                                        p1Area++;
                                }
                                //Jucatorul 2 ajunge primul la acest nod, deci el este detinatorul
                                else if(p1Graph[i][j].getDiscoverTime() >
                                         p2Graph[i][j].getDiscoverTime()) {
                                        resultGraph[i][j] = new Node(p2Graph[i][j]);
                                        p2Area++;
                                }
                                //In acest caz timpii sunt egali. Trebuie sa marcam nodul ca fiind
                                //neutru.
                                else {
                                        //Am ales arbitrar sa-i atribui valoarea lui p1Graph[i][j].
                                        //Puteam la fel de bine sa aleg p2Graph[i][j].
                                        //Important este ca ambii jucatori ajung in acelasi
                                        //timp la nod.
                                        resultGraph[i][j] = new Node(p1Graph[i][j]);
                                        //Marchez ca nodul este neutru.
                                        resultGraph[i][j].setOwner(Owner.NEUTRAL);
                                }
                Pair<Integer, Integer> areas = new Pair<Integer, Integer>();
                areas.a = p1Area;
                areas.b = p2Area;
                Pair<Node[][], Pair<Integer, Integer> > res =
                                                     new Pair<Node[][], Pair<Integer, Integer> >();
                res.a = resultGraph;
                res.b = areas;
                return res;
        }

        //Un algoritm de explorare de tipul bfs.
        private void fill(Node[][] graph, Position startingPos, Owner player) {
                if(startingPos.getLine() < 0 || startingPos.getLine() >= height ||
                   startingPos.getColumn() < 0 || startingPos.getColumn() >= width)
                        return;
                //Initializeaza coada de explorare cu pozitia de start
                Queue<Position> toExplore = new LinkedList<Position>();
                toExplore.offer(startingPos);
                //Marcheaza nodul de start ca fiind descoperit si ii seteaza detinatorul
                graph[startingPos.getLine()][startingPos.getColumn()].markDiscovery(0);
                graph[startingPos.getLine()][startingPos.getColumn()].setOwner(player);
                while(!toExplore.isEmpty()) {
                        Position currentPos = toExplore.poll();
                        int time = graph[currentPos.getLine()][currentPos.getColumn()].getDiscoverTime() + 1;
                        for(Position neighbour : currentPos.getAllNeighbours()) {
                                //Verific sa nu ies in afara limitelor matricei
                                if(neighbour.getLine() < 0 ||
                                   neighbour.getLine() >= height ||
                                   neighbour.getColumn() < 0 ||
                                   neighbour.getColumn() >= width)
                                        continue;
                                //Daca nodul nu a mai fost explorat si este gol (caracterul '-'), 
                                //apelez algoritmul de explorare si pentru el.
                                if(graph[neighbour.getLine()][neighbour.getColumn()].getColour() == 
                                   Colour.WHITE &&
                                   graph[neighbour.getLine()][neighbour.getColumn()].getSymbol() ==
                                   emptySymbol) {
                                        graph[neighbour.getLine()][neighbour.getColumn()].markDiscovery(time);
                                        graph[neighbour.getLine()][neighbour.getColumn()].setOwner(player);
                                        toExplore.offer(neighbour);
                                }
                        }
                }
        }

        //Exploram componenta conexa din care face parte nodul de pe pozitia pos
        //In urma explorarii marcam proprietarul (owner) nodurilor explorate si timpul la care au fost
        //descoperite. Cu cat timpul este mai mic, cu atat proprietarul poate sa ajunga mai repede
        //acolo. Consideram o componenta conexa zona de pe harta pe care o putem explora (deci toate
        //nodurile trebuie sa fie marcate cu simbolul '-')
        private Node[][] connectedComponent(Position pos, Owner player) {
                if(pos.getLine() < 0 || pos.getLine() > height ||
                   pos.getColumn() < 0 || pos.getColumn() > width)
                        return null;
                Node[][] graph = new Node[height][width];
                for(int i = 0; i < height; i++)
                        for(int j = 0; j < width; j++)
                                graph[i][j] = new Node(map[i][j]);
                fill(graph, pos, player);
                return graph;
        }

        //Folosit pentru debug
        @Override
        public String toString() {
                String toStr = "Width: %d, Height: %d\nPlayer 1 position:\nLine: %d, Column: %d\n" +
                                           "Player 2 position:\nLine: %d, Column: %d\n";
                toStr = String.format(toStr, height, width, player1.getLine(), player1.getColumn(),
                                                          player2.getLine(), player2.getColumn());
                for(int i = 0; i < height; i++) {
                        for(int j = 0; j < width; j++)
                                toStr += Character.toString(map[i][j]);
                        toStr += "\n";
                }
                toStr = toStr.trim();
                return toStr;
        }
}


class Node {
        public final static int INF = Integer.MAX_VALUE;
        //Jucatorul care ajunge primul la nod este detinatorul nodului
        private Owner nodeOwner;
        //Culoarea este folosita pentru algoritmii de parcurgere a grafurilor
        private Colour colour;
        //Timpul la care a fost descoperit
        private int discoverTime;
        //Simbolul nodului in harta (- / # / g / r)
        private char symbol;

        //Construieste un nod pornind de la o pozitie
        public Node(char sym) {
                nodeOwner = Owner.NEUTRAL;
                colour = Colour.WHITE;
                discoverTime = Node.INF;
                symbol = sym;
        }

        //Copy constructor
        public Node(Node old) {
                nodeOwner = old.getOwner();
                colour = old.getColour();
                discoverTime = old.getDiscoverTime();
                symbol = old.getSymbol();
        }

        //Seteaza detinatorul unui nod
        public void setOwner(Owner own) {
                nodeOwner = own;
        }

        //Seteaza simbolul unui nod
        public void setSymbol(char sym) {
                symbol = sym;
        }

        //Intoarce detinatorul nodului
        public Owner getOwner() {
                return nodeOwner;
        }

        //Intoarce simbolul nodului
        public char getSymbol() {
                return symbol;
        }

        //Intoarce culoarea nodului
        public Colour getColour() {
                return colour;
        }

        //Intoarce timpul de descoperire
        public int getDiscoverTime() {
                return discoverTime;
        }

        //Marcheaza nodul ca fiind descoperit
        public void markDiscovery(int time) {
                colour = Colour.GREY;
                discoverTime = time;
        }
}

class Bot {
        private final static int INF = Integer.MAX_VALUE;
        private final static int maxDepth = 4;
        private Direction chosen;
        //Jucatorul pe care bot-ul il controleaza
        private Owner player;
        //Arena in care se desfasoara lupta
        private Arena battleArena;

        private int alphabeta(Arena ar, Owner pl, int depth) {
                if(ar.getFreeNeighbours(ar.getPlayerPosition(pl)).isEmpty())
                        return -INF;
                if(ar.getFreeNeighbours(ar.getPlayerPosition(opponent(pl))).isEmpty() && depth != 1)
                        return INF;
                if(ar.getPlayerPosition(pl).getLine() == ar.getPlayerPosition(opponent(pl)).getLine() &&
                   ar.getPlayerPosition(pl).getColumn() == ar.getPlayerPosition(opponent(pl)).getColumn())
                        return -INF + 10;
                if(depth > maxDepth * 2) {
                        Pair<Node[][], Pair<Integer, Integer> > score =
                                        ar.computeTerritories(ar.getPlayerPosition(Owner.PLAYER1),
                                                              ar.getPlayerPosition(Owner.PLAYER2));
                        if(player == Owner.PLAYER1)
                                return score.b.a - score.b.b;
                        else
                                return score.b.b - score.b.a;
                }
                if(depth == 1)
                                chosen = Direction.NORTH;
                int best = -INF;
                for(Direction d : ar.getFreeNeighbours(ar.getPlayerPosition(pl))) {
                        int result = -alphabeta(ar.move(pl, d), opponent(pl), depth + 1);
                        if(result > best) {
                                best = result;
                                if(depth == 1)
                                        chosen = d;
                        }
                }
                return best;
        }

        private Owner opponent(Owner pl) {
                if(pl == Owner.PLAYER1)
                        return Owner.PLAYER2;
                if(pl == Owner.PLAYER2)
                        return Owner.PLAYER1;
                return Owner.NEUTRAL;
        }

        public Bot(char pl, Arena ar) {
                player = (pl == 'r' ? Owner.PLAYER1 : Owner.PLAYER2);
                battleArena = ar;
        }

        public Direction makeChoice() {
                alphabeta(battleArena, player, 1);
                return chosen;
        }
}

public class Solution {

        static void nextMove(String player, int[] pos, String[] board) {
                //Am folosit lungimea lui board[0] ca sa aflu lungimea unei linii a matricei.
                //Cum toate liniile sunt egale in lungime, am ales-o pe prima.
                char[][] map = new char[board.length][board[0].length()];
                //Copiez board in matricea map
                for(int i = 0; i < board.length; i++)
                        map[i] = board[i].toCharArray();
                Bot tron = new Bot(player.charAt(0),
                                           new Arena(map, board.length, board[0].length(),
                                                     new Position(pos[0], pos[1]),
                                                     new Position(pos[2], pos[3])));
                //Aleg directia in care sa ma misc
                Direction chosen = tron.makeChoice();
                //Scriu rezultatul
                if(chosen == Direction.NORTH)
                        System.out.print("UP");
                else if(chosen == Direction.EAST)
                        System.out.print("RIGHT");
                else if(chosen == Direction.SOUTH)
                        System.out.print("DOWN");
                else if(chosen == Direction.WEST)
                        System.out.print("LEFT");
        }


        public static void main(String[] args) {
                try {
                        BufferedReader consoleIn = new BufferedReader
                                                                 (new InputStreamReader(System.in));

                        //Citeste linia cu jucatorul al carui rand este
                        String player = consoleIn.readLine();

                        //Citeste pozitiile celor doi jucatori
                        String positions = consoleIn.readLine();
                        //Imparte linia cu coordonatele in mai multe siruri, fiecare reprezentand un
                        //numar.
                        String[] coords = positions.split(" ");
                        //Transforma vectorul de coordonate din format String in vector de tipul int.
                        int[] parsedCoords = new int[4];
                        for(int i = 0; i < 4; i++)
                                parsedCoords[i] = Integer.parseInt(coords[i]);

                        //Citeste dimensiunea hartii
                        String mapSize = consoleIn.readLine();
                        //Imparte linia cu marimile in doua siruri, fiecare reprezentand un numar.
                        String[] sizes = mapSize.split(" ");
                        //Transforma marimile in int
                        int[] parsedSizes = new int[2];
                        for(int i = 0; i < 2; i++)
                                parsedSizes[i] = Integer.parseInt(sizes[i]);

                        //Citeste intreaga harta
                        String[] map = new String[parsedSizes[0]];
                        for(int i = 0; i < parsedSizes[0]; i++)
                                map[i] = consoleIn.readLine();

                        //Apeleaza functia de alegere a miscarii
                        nextMove(player, parsedCoords, map);
                }
                catch (IOException e) {
                        e.printStackTrace();
                }
        }
}
